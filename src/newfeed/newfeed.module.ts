/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewFeedEntity } from '../entities/newfeed.entity';
import { NewFeedController } from './newfeed.controller';
import { NewFeedService } from './newfeed.service';
import { UserEntity } from '../entities/users.entity';
import { ImagesEntity } from './../entities/images.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([NewFeedEntity, UserEntity, ImagesEntity]),
  ],
  controllers: [NewFeedController],
  providers: [NewFeedService],
})
export class NewFeedModule {}
