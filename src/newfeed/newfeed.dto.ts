import { IsNotEmpty } from 'class-validator';
import { ImagesEntity } from '../entities/images.entity';
import { UserEntity } from '../entities/users.entity';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
export interface NewFeed {
  id?: number;
  title?: string;
  description?: string;
  location?: string;
  like?: number;
  createdAt?: Date;
  deleteAt?: Date;
  user?: UserEntity;
  images?: ImagesEntity[];
}
export class NewFeedDTO implements NewFeed {
  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  userId: number;

  @ApiPropertyOptional()
  location?: string;

  // file
}
