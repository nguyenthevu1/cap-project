import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NewFeed } from './newfeed.dto';
import { NewFeedEntity } from '../entities/newfeed.entity';
import { UserEntity } from '../entities/users.entity';
import { AuthJwtPayload } from '../types/authJwtPayload';
import { ImagesEntity } from './../entities/images.entity';

@Injectable()
export class NewFeedService {
  constructor(
    @InjectRepository(NewFeedEntity)
    private newFeedRepository: Repository<NewFeedEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    @InjectRepository(ImagesEntity)
    private imageRepository: Repository<ImagesEntity>,
  ) {}

  async createAsync(userReq: AuthJwtPayload, data: NewFeed, imagePath: string) {
    const userId = userReq.userId;
    const user = await this.userRepository.findOneBy({ id: userId });

    data.user = user;

    const newFeed = await this.newFeedRepository.create(data);

    if (user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    const imageNewFeed = await this.imageRepository.create();
    imageNewFeed.image = imagePath;
    imageNewFeed.newFeed = newFeed;

    await this.newFeedRepository.save(newFeed);
    await this.imageRepository.save(imageNewFeed);

    return {
      success: true,
    };
  }

  async getAllASync() {
    const newFeeds = await this.newFeedRepository.find();
    return {
      newFeeds: newFeeds,
    };
  }

  async getSingleAsync(id: number) {
    const newFeed = await this.newFeedRepository.findOneBy({ id: id });
    if (!newFeed)
      throw new HttpException(
        'New Feed does not exits',
        HttpStatus.BAD_REQUEST,
      );

    return {
      newFeed,
    };
  }

  async updateAsync(id: number, data: NewFeed) {
    const newFeed = await this.newFeedRepository.findOneBy({ id: id });
    if (!newFeed)
      throw new HttpException(
        'New Feed does not exits',
        HttpStatus.BAD_REQUEST,
      );

    await this.newFeedRepository.update({ id: id }, data);

    return {
      success: true,
    };
  }

  async deleteAsync(id: number) {
    const newFeed = await this.newFeedRepository.findOneBy({ id: id });
    if (!newFeed)
      throw new HttpException(
        'New Feed does not exits',
        HttpStatus.BAD_REQUEST,
      );

    await this.newFeedRepository.softDelete(id);
    return {
      success: true,
    };
  }
}
