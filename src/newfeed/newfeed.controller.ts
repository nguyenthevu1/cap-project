import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseFilePipe,
  Post,
  Put,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
// import { AuthGuard } from '../helpers/auth.guard';
import storage from '../helpers/storage';
import { ValidationPipe } from '../helpers/validation.pipe';
import { NewFeedDTO } from './newfeed.dto';
import { NewFeedService } from './newfeed.service';

@ApiSecurity('JWT-auth')
@ApiTags('New Feed')
// @UseGuards(new AuthGuard())
@Controller('api/newfeed')
export class NewFeedController {
  constructor(private newFeedService: NewFeedService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  @UseInterceptors(FileInterceptor('file', storage))
  createNewFeed(
    @Body() data: NewFeedDTO,
    @Req() req,
    @UploadedFile(new ParseFilePipe()) file: Express.Multer.File,
  ) {
    console.log(file);
    return this.newFeedService.createAsync(req.user, data, file.path);
  }

  @Get()
  allNewFeed() {
    return this.newFeedService.getAllASync();
  }

  @Get(':id')
  singleNewFeed(@Param('id') id: number) {
    return this.newFeedService.getSingleAsync(id);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  updateNewFeed(@Param('id') id: number, @Body() data: NewFeedDTO) {
    return this.newFeedService.updateAsync(id, data);
  }

  @Delete(':id')
  deleteNewFeed(@Param('id') id: number) {
    return this.newFeedService.deleteAsync(id);
  }
}
