import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { APP_FILTER } from '@nestjs/core';

import { UsersModule } from './users/users.module';
import { RoomsModule } from './rooms/rooms.module';
import { NewFeedModule } from './newfeed/newfeed.module';
import { ImagesModule } from './images/images.module';
import { MilestoneModule } from './milestone/milestone.module';
import { ChatsModule } from './chats/chats.module';

import { config } from '../orm.config';
import { HttpExceptionFilter } from './helpers/httpException.filter';
import { AppGateway } from './app.gateway';

@Module({
  imports: [
    TypeOrmModule.forRoot(config),
    UsersModule,
    RoomsModule,
    NewFeedModule,
    ImagesModule,
    MilestoneModule,
    ChatsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    AppGateway,
  ],
})
export class AppModule {}
