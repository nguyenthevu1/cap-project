import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
// import { AuthGuard } from '../helpers/auth.guard';
import { Chat, ChatDTO } from './chat.dto';
import { ChatsService } from './chats.service';

@ApiSecurity('JWT-auth')
@ApiTags('Chats')
// @UseGuards(new AuthGuard())
@Controller('api/chats')
export class ChatsController {
  constructor(private chatService: ChatsService) {}

  @Post()
  createChat(@Body() data: ChatDTO, @Req() req) {
    return this.chatService.createAsync(req.user, data);
  }

  @Put(':id')
  updateChat(@Param('id') id: string, @Body() data: Chat) {
    return this.chatService.updateAsync(id, data);
  }

  @Delete(':id')
  deleteChat(@Param('id') id: string) {
    return this.chatService.deleteAsync(id);
  }

  @Get()
  allChat() {
    return this.chatService.getAllChat();
  }

  @Get('restore/:id')
  restoreChat(@Param('id') id: string) {
    return this.chatService.restore(id);
  }
}
