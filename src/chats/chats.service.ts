import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChatEntity } from '../entities/chats.entity';
import { Chat } from './chat.dto';
import { AuthJwtPayload } from 'src/types/authJwtPayload';
import { UserEntity } from '../entities/users.entity';
import { RoomEntity } from '../entities/rooms.entity';

@Injectable()
export class ChatsService {
  constructor(
    @InjectRepository(ChatEntity)
    private chatRepository: Repository<ChatEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(RoomEntity)
    private roomRepository: Repository<RoomEntity>,
  ) {}

  async createAsync(userReq: AuthJwtPayload, data: Chat) {
    const userId = userReq.userId;
    const roomId = data.id;
    const user = await this.userRepository.findOneBy({ id: userId });
    const room = await this.roomRepository.findOneBy({ id: roomId });

    if (!user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    if (!room)
      throw new HttpException('Room does not exits', HttpStatus.BAD_REQUEST);

    data.user = user;
    data.room = room;

    const chat = await this.chatRepository.create(data);
    await this.chatRepository.save(chat);
    return { success: true };
  }

  async deleteAsync(id: string) {
    await this.chatRepository.softDelete({ id: Number(id) });
    return true;
  }

  async updateAsync(id: string, data) {
    await this.chatRepository.update({ id: Number(id) }, data);

    return {
      success: true,
    };
  }

  async getAllChat() {
    const chats = await this.chatRepository.find();
    return {
      chats,
    };
  }

  async restore(id: string) {
    await this.chatRepository.restore(Number(id));
    return { success: true };
  }
}
