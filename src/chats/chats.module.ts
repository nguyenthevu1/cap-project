import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatsController } from './chats.controller';
import { ChatsService } from './chats.service';
import { ChatEntity } from '../entities/chats.entity';
import { UserEntity } from '../entities/users.entity';
import { RoomEntity } from '../entities/rooms.entity';
import { ChatGateway } from './chat.gateway';
import { RoomsModule } from './../rooms/rooms.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ChatEntity, UserEntity, RoomEntity]),
    RoomsModule,
  ],
  controllers: [ChatsController],
  providers: [ChatsService, ChatGateway],
})
export class ChatsModule {}
