import { UserEntity } from '../entities/users.entity';
import { RoomEntity } from '../entities/rooms.entity';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export interface Chat {
  id?: number;
  message?: string;
  user?: UserEntity;
  room?: RoomEntity;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}

export class ChatDTO implements Chat {
  @ApiProperty()
  @IsNotEmpty()
  message: string;
}
