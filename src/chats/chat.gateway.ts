import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';

import { Socket, Server } from 'socket.io';
import { v4 as uuidv4 } from 'uuid';
import { RoomsService } from './../rooms/rooms.service';
import { RoomEntity } from './../entities/rooms.entity';
import { Repository } from 'typeorm';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class ChatGateway {
  private logger: Logger = new Logger('ChatGateway');
  constructor(
    @InjectRepository(RoomEntity)
    private roomRepository: Repository<RoomEntity>,
  ) {}

  @WebSocketServer()
  server: Server;

  @SubscribeMessage('createRoom')
  async handleCreateRoom(client: Socket, payload: string) {
    const room = {};
    const roomId = uuidv4();
    room[roomId] = payload;

    const newRoom = await this.roomRepository.create(room);
    await this.roomRepository.save(newRoom);

    client.emit('created', room);
  }

  @SubscribeMessage('message')
  handleMessage(client: Socket, payload: any) {
    client.emit('message', 'test');
  }
}
