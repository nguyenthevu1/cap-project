/* eslint-disable prettier/prettier */
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './users.entity';
import { NewFeedEntity } from './newfeed.entity';
import { MilestoneEntity } from './milestone.entity';

@Entity('images')
export class ImagesEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  // @Column({ nullable: true })
  @ManyToOne(() => UserEntity, (user) => user.images)
  user: UserEntity;

  @ManyToOne(() => NewFeedEntity, (newFeed) => newFeed.images)
  // @Column({ nullable: true })
  newFeed: NewFeedEntity;

  @ManyToOne(() => MilestoneEntity, (milestone) => milestone.images)
  // @Column({ nullable: true })
  milestone: MilestoneEntity;
}
