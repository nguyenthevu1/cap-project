/* eslint-disable prettier/prettier */
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserEntity } from './users.entity';
import { RoomEntity } from './rooms.entity';

@Entity('chats')
export class ChatEntity {
  @PrimaryGeneratedColumn()
  id;

  @Column({ type: 'text' })
  message: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @ManyToOne(() => UserEntity, (user) => user.chats)
  user: UserEntity;

  @ManyToOne(() => RoomEntity, (room) => room.chats)
  room: RoomEntity;
}
