/* eslint-disable prettier/prettier */
import { UserEntity } from './users.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ImagesEntity } from './images.entity';

@Entity('newFeed')
export class NewFeedEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  location: string;

  @Column({
    type: 'text',
  })
  description: string;

  @Column({
    nullable: true,
    default: 0,
  })
  like: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @ManyToOne(() => UserEntity, (user) => user.newFeed)
  user: UserEntity;

  @OneToMany(() => ImagesEntity, (image) => image.newFeed)
  images: ImagesEntity[];

  toResponseObject() {
    return this;
  }
}
