import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { RoomEntity } from './rooms.entity';
import { UserEntity } from './users.entity';

@Entity('userRoom')
export class UserRoomEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => RoomEntity, (room) => room.roomUsers)
  room: RoomEntity;

  @ManyToOne(() => UserEntity, (user) => user.userRooms)
  user: UserEntity;
}
