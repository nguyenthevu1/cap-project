import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatEntity } from './chats.entity';
import { UserRoomEntity } from './userRoom.entity';
import { UserEntity } from './users.entity';
@Entity('rooms')
export class RoomEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  roomSocket: string;

  @Column({ nullable: false })
  roomName: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(() => ChatEntity, (chat) => chat.room)
  chats: ChatEntity[];

  @OneToMany(() => UserRoomEntity, (roomUser) => roomUser.room)
  roomUsers: UserRoomEntity[];

  @ManyToOne(() => UserEntity, (room) => room.room)
  user: UserEntity;
}
