import { NewFeedEntity } from './newfeed.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { MilestoneEntity } from './milestone.entity';
import { ImagesEntity } from './images.entity';
import { ChatEntity } from './chats.entity';
import { RoomEntity } from './rooms.entity';
import { UserDTO } from '../users/users.dto';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { Secret } from 'jsonwebtoken';
import { UserRoomEntity } from './userRoom.entity';
import { UserMilestone } from './userMilestone.entity';
@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  numberPhone: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  resetPasswordToken: string;

  @Column({ type: 'date', nullable: true })
  resetPasswordExpire: Date;

  @Column({ nullable: true })
  otp: number;

  @Column({ nullable: true, type: 'bigint' })
  otpExpire: number;

  @Column({ nullable: true, default: false })
  verify: boolean;

  @Column({ type: 'date' })
  dateOfBirth: Date;

  @Column()
  gender: boolean;

  @Column({ nullable: true })
  refreshToken: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(() => NewFeedEntity, (newFeed) => newFeed.user)
  newFeed: NewFeedEntity[];

  @OneToMany(() => MilestoneEntity, (milestones) => milestones.user)
  milestones: MilestoneEntity[];

  @OneToMany(() => ImagesEntity, (image) => image.user)
  images: ImagesEntity[];

  @OneToMany(() => ChatEntity, (chat) => chat.user)
  chats: ChatEntity[];

  @OneToMany(() => RoomEntity, (room) => room.user)
  room: RoomEntity[];

  @OneToMany(() => UserRoomEntity, (userRoom) => userRoom.user)
  userRooms: UserRoomEntity[];

  @OneToMany(
    () => UserMilestone,
    (userMilestone) => userMilestone.userMilestone,
  )
  userMilestones: UserMilestone[];

  toResponseObject(): UserDTO {
    const {
      id,
      numberPhone,
      firstName,
      lastName,
      dateOfBirth,
      gender,
      createdAt,
      updatedAt,
      deleteAt,
    } = this;

    const responseObject = {
      id,
      numberPhone,
      firstName,
      lastName,
      dateOfBirth,
      gender,
      createdAt,
      updatedAt,
      deleteAt,
    };
    return responseObject;
  }

  comparePassword(password: string): boolean {
    return bcrypt.compareSync(password, this.password);
  }

  createToken = (type: 'accessToken' | 'refreshToken', user: UserDTO) =>
    jwt.sign(
      {
        userId: user.id,
      },
      type === 'accessToken'
        ? (process.env.ACCESS_TOKEN_SECRET as Secret)
        : (process.env.REFRESH_TOKEN_SECRET as Secret),
      { expiresIn: type === 'accessToken' ? '40s' : '24h' },
    );
}
