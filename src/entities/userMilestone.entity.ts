/* eslint-disable prettier/prettier */
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from './users.entity';

@Entity('userMilestone')
export class UserMilestone {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity, (user) => user.userMilestones)
  userMilestone: UserEntity;
}
