/* eslint-disable prettier/prettier */
import { diskStorage } from 'multer';
import { v4 as uuidV4 } from 'uuid';
import path = require('path');

const storage = {
  storage: diskStorage({
    destination: '../Cap/src/uploads',
    filename: (req, file, cb) => {
      const fileName: string =
        path.parse(file.originalname).name.replace(/\s/g, '') + uuidV4();
      const extension: string = path.parse(file.originalname).ext;

      cb(null, `${fileName}${extension}`);
    },
  }),
};

export default storage;
