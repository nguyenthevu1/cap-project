import { JwtPayload } from 'jsonwebtoken';

export type AuthJwtPayload = JwtPayload & { userId: number };
