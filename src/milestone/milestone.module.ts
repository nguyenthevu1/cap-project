import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MilestoneController } from './milestone.controller';
import { MilestoneService } from './milestone.service';
import { UserEntity } from '../entities/users.entity';
import { MilestoneEntity } from '../entities/milestone.entity';
import { UserMilestone } from './../entities/userMilestone.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([MilestoneEntity, UserEntity, UserMilestone]),
  ],
  controllers: [MilestoneController],
  providers: [MilestoneService],
})
export class MilestoneModule {}
