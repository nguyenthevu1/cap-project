import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
// import { AuthGuard } from '../helpers/auth.guard';
import { ValidationPipe } from './../helpers/validation.pipe';
import { MilestoneDTO } from './milestone.dto';
import { MilestoneService } from './milestone.service';

@ApiSecurity('JWT-auth')
@ApiTags('Milestone')
// @UseGuards(new AuthGuard())
@Controller('api/milestones')
export class MilestoneController {
  constructor(private milestoneService: MilestoneService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  createMilestone(@Body() data: MilestoneDTO, @Req() req) {
    return this.milestoneService.createMilestoneAsync(req.user, data);
  }

  @Get()
  allMilestone() {
    return this.milestoneService.getAllMilestone();
  }

  @Get(':id')
  singleMilestone(@Param('id') id: string) {
    return this.milestoneService.getSingleMilestone(id);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  updateMilestone(@Param('id') id: string, @Body() data: MilestoneDTO) {
    return this.milestoneService.updateAsync(id, data);
  }

  @Delete(':id')
  deleteMilestone(@Param('id') id: string) {
    return this.milestoneService.deleteAsync(id);
  }
}
