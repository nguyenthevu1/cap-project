import { UserEntity } from '../entities/users.entity';
import { ImagesEntity } from '../entities/images.entity';
import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export interface Milestone {
  id?: number;
  title?: string;
  location?: string;
  content?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deleteAt?: Date;
  user?: UserEntity;
  images?: ImagesEntity[];
  userMilestone?: UserEntity[];
}

export class MilestoneDTO implements Milestone {
  @ApiProperty()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  content: string;

  @ApiProperty()
  @IsNotEmpty()
  location: string;
}
