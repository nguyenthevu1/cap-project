import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Milestone } from './milestone.dto';
import { MilestoneEntity } from '../entities/milestone.entity';
import { UserEntity } from '../entities/users.entity';
import { AuthJwtPayload } from '../types/authJwtPayload';
import { UserMilestone } from './../entities/userMilestone.entity';

@Injectable()
export class MilestoneService {
  constructor(
    @InjectRepository(MilestoneEntity)
    private milestoneRepository: Repository<MilestoneEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(UserMilestone)
    private userMilestoneRepository: Repository<UserMilestone>,
  ) {}

  async createMilestoneAsync(userReq: AuthJwtPayload, data: Milestone) {
    const userId = userReq.userId;
    const user = await this.userRepository.findOneBy({ id: userId });

    if (!user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    data.user = user;
    const milestone = await this.milestoneRepository.create(data);

    if (!user)
      throw new HttpException(
        'Milestone does not exits',
        HttpStatus.BAD_REQUEST,
      );

    const userMilestone = await this.userMilestoneRepository.create();
    userMilestone.userMilestone = user;

    await this.milestoneRepository.save(milestone);
    await this.userMilestoneRepository.save(userMilestone);

    return { success: true };
  }

  async getAllMilestone() {
    const milestones = await this.milestoneRepository.find();
    return {
      milestones,
    };
  }

  async getSingleMilestone(id: string) {
    const milestone = await this.milestoneRepository.findOneBy({
      id: Number(id),
    });

    if (!milestone)
      throw new HttpException(
        'Milestone does not exits',
        HttpStatus.BAD_REQUEST,
      );

    return {
      milestone,
    };
  }

  async updateAsync(id: string, data: Milestone) {
    // const
    const milestone = await this.milestoneRepository.findOneBy({
      id: Number(id),
    });

    if (!milestone)
      throw new HttpException(
        'Milestone does not exits',
        HttpStatus.BAD_REQUEST,
      );

    await this.milestoneRepository.update({ id: Number(id) }, data);

    return {
      success: true,
    };
  }

  async deleteAsync(id: string) {
    const milestone = await this.milestoneRepository.findOneBy({
      id: Number(id),
    });

    if (!milestone)
      throw new HttpException(
        'Milestone does not exits',
        HttpStatus.BAD_REQUEST,
      );

    await this.milestoneRepository.softDelete(Number(id));
    return {
      success: true,
    };
  }
}
