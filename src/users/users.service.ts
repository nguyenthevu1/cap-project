import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/users.entity';
import { Repository } from 'typeorm';
import { UserDTO, UserDO, UserRegister } from './users.dto';
import * as bcrypt from 'bcryptjs';
import { User } from '../users/users.dto';
import { AuthJwtPayload } from 'src/types/authJwtPayload';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService,
  ) {}

  async loginAsync(data: UserDO) {
    const { numberPhone, password } = data;

    const user = await this.userRepository.findOneBy({
      numberPhone: numberPhone,
    });

    if (!user || !user.comparePassword(password)) {
      throw new UnauthorizedException('User does not exits or wrong password');
    }

    if (!user.verify)
      throw new HttpException('Otp does not verify', HttpStatus.BAD_REQUEST);

    const userId = user.id;
    const accessToken = await this.jwtService.sign({ id: userId });

    return {
      user: user.toResponseObject(),
      accessToken,
    };
  }

  // async validateUser(userId) {
  //   // const user = await this.userRepository.findOneBy({id:user})
  //   return;
  // }

  async registerAsync(
    data: UserRegister,
  ): Promise<{ user: UserRegister; success: boolean }> {
    const { confirmPassword, password, numberPhone, ...other } = data;

    const user = await this.userRepository.findOneBy({
      numberPhone: numberPhone,
    });

    if (user)
      throw new HttpException('User already exist', HttpStatus.BAD_REQUEST);

    if (password !== confirmPassword)
      throw new HttpException(
        'ConfirmPassword not match Password',
        HttpStatus.BAD_REQUEST,
      );

    const { otp, otpExpire } = this.generateOtp();

    const hashPass = this.hashPassword(password);

    const newUser = await this.userRepository.create({
      password: hashPass,
      numberPhone,
      otpExpire,
      otp,
      ...other,
    });

    await this.userRepository.save(newUser);

    return {
      user: newUser.toResponseObject(),
      success: true,
    };
  }

  async refreshTokenAsync(data) {
    const user = await this.userRepository.findOneBy({
      refreshToken: data.refToken,
    });

    if (!user)
      throw new HttpException('Please login again', HttpStatus.BAD_REQUEST);

    const accessToken = user.createToken('accessToken', user);

    return {
      success: true,
      accessToken,
    };
  }

  async resendOtpAsync(data) {
    const user = await this.userRepository.findOneBy({
      id: Number(data.userId),
    });
    if (!user)
      throw new HttpException(
        'User does not exits',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );

    const { otp, otpExpire } = this.generateOtp();
    await this.userRepository.update(
      { id: Number(data.userId) },
      { otp, otpExpire },
    );
    return {
      success: true,
    };
  }

  async verifyOtp(data, userId: number) {
    const user = await this.userRepository.findOneBy({ id: userId });

    if (!user)
      throw new HttpException(
        'User does not exits',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );

    const currentDate = Date.now();

    if (user.otpExpire > currentDate && data.otp == user.otp) {
      await this.userRepository.update(
        { id: userId },
        {
          otp: null,
          otpExpire: null,
          verify: true,
        },
      );
      return { success: true };
    } else {
      return { success: false };
    }
  }

  async showAllUserAsync(): Promise<{ users: UserDTO[] }> {
    const users = await this.userRepository.find();

    return {
      users: users.map((user) => user.toResponseObject()),
    };
  }

  async singleUserAsync(id: number) {
    const user = await this.userRepository.findOneBy({ id: id });

    if (!user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    return {
      success: true,
      user: user.toResponseObject(),
    };
  }

  async updateAsync(id: number, data: UserDTO) {
    const user = await this.userRepository.findOneBy({ id: id });

    if (!user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    await this.userRepository.update({ id: id }, { ...data });

    return {
      success: true,
    };
  }

  async deleteAsync(id) {
    const user = await this.userRepository.findOneBy({ id: id });
    if (!user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);

    await this.userRepository.softDelete({ id: id });
    return {
      success: true,
    };
  }

  async logoutAsync(userReq: AuthJwtPayload) {
    await this.userRepository.update(
      { id: userReq.userId },
      {
        refreshToken: null,
      },
    );
    return {
      success: true,
    };
  }

  async validateUser(id: number) {
    const user = await this.userRepository.findOneBy({ id: id });

    console.log(user);
    if (!user) {
      return null;
    }

    return user;
  }

  hashPassword(password: string) {
    const salt = bcrypt.genSaltSync(10);
    const hashPass = bcrypt.hashSync(password, salt);
    return hashPass;
  }

  generateOtp() {
    const otp = Math.floor(Math.random() * 10000000);
    const otpExpire = Date.now() + 60 * 1000;

    return {
      otp,
      otpExpire,
    };
  }
}
