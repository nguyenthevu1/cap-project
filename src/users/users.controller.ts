import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { User } from '../decorator/CurrentUser.decorator';
import { ValidationPipe } from '../helpers/validation.pipe';
import { UserDO, UserDTO, UserRegister } from './users.dto';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
@ApiTags('User')
@Controller('api/users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post('login')
  @UsePipes(new ValidationPipe())
  login(@Body() data: UserDO) {
    return this.userService.loginAsync(data);
  }

  @Post('register')
  @UsePipes(new ValidationPipe())
  register(@Body() data: UserRegister) {
    return this.userService.registerAsync(data);
  }

  @Post('refreshToken')
  refreshToken(@Body() data) {
    return this.userService.refreshTokenAsync(data);
  }

  @ApiSecurity('JWT-auth')
  @Get('')
  @UseGuards(AuthGuard('jwt'))
  showAllUser() {
    return this.userService.showAllUserAsync();
  }

  @ApiSecurity('JWT-auth')
  @Get(':id')
  singleUser(@Param('id') id: number) {
    return this.userService.singleUserAsync(id);
  }

  @ApiSecurity('JWT-auth')
  @Put()
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  updateUser(@Body() data: UserDTO, @User() user) {
    return this.userService.updateAsync(user.userId, data);
  }

  @Post('verify/:id')
  verifyOtp(@Body() data, @Param('id') id: number) {
    return this.userService.verifyOtp(data, Number(id));
  }

  @Post('resend')
  resendOtp(@Body() data) {
    return this.userService.resendOtpAsync(data);
  }

  @ApiSecurity('JWT-auth')
  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  deleteUser(@Param('id') id: number) {
    return this.userService.deleteAsync(id);
  }

  @ApiSecurity('JWT-auth')
  @Post('logout')
  @UseGuards(AuthGuard('jwt'))
  logout(@User() user) {
    return this.userService.logoutAsync(user);
  }
}
