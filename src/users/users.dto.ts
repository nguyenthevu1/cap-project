import { IsNotEmpty, Length } from 'class-validator';
import { MilestoneEntity } from '../entities/milestone.entity';
import { NewFeedEntity } from '../entities/newfeed.entity';
import { ImagesEntity } from '../entities/images.entity';
import { ChatEntity } from '../entities/chats.entity';
import { RoomEntity } from '../entities/rooms.entity';
import { ApiProperty } from '@nestjs/swagger';
export interface User {
  id?: number;
  numberPhone?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  resetPasswordToken?: string;
  resetPasswordExpire?: Date;
  dateOfBirth?: Date;
  gender?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  newFeed?: NewFeedEntity[];
  milestones?: MilestoneEntity[];
  images?: ImagesEntity[];
  chats?: ChatEntity[];
  rooms?: RoomEntity[];
  milestoneUser?: MilestoneEntity[];
}

export class UserDO implements User {
  @ApiProperty()
  @IsNotEmpty()
  numberPhone: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
  refreshToken?: string;
  accessToken?: string;
}

export class UserDTO implements User {
  id?: number;

  @ApiProperty()
  @IsNotEmpty()
  numberPhone: string;

  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsNotEmpty()
  dateOfBirth: Date;

  @ApiProperty()
  @IsNotEmpty()
  gender: boolean;
}

export class UserRegister extends UserDTO {
  @IsNotEmpty()
  @Length(6)
  password?: string;

  @IsNotEmpty()
  confirmPassword?: string;
}
