import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoomEntity } from '../entities/rooms.entity';
import { Room } from './room.dto';
import { AuthJwtPayload } from '../types/authJwtPayload';
import { UserEntity } from '../entities/users.entity';
import { UserRoomEntity } from './../entities/userRoom.entity';

@Injectable()
export class RoomsService {
  constructor(
    @InjectRepository(RoomEntity)
    private roomRepository: Repository<RoomEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(UserRoomEntity)
    private userRoomRepository: Repository<UserRoomEntity>,
  ) {}

  async createAsync(userReq: AuthJwtPayload, data: Room) {
    const room = await this.roomRepository.findOneBy({
      roomName: data.roomName,
    });

    if (room)
      throw new HttpException('Room already exits', HttpStatus.BAD_REQUEST);

    const userId = userReq.userId;

    const user = await this.userRepository.findOneBy({ id: Number(userId) });

    if (user)
      throw new HttpException('User does not exits', HttpStatus.BAD_REQUEST);
    data.user = user;

    const newRoom = await this.roomRepository.create(data);

    const userRoom = await this.userRoomRepository.create();
    userRoom.room = newRoom;
    userRoom.user = user;

    await this.roomRepository.save(newRoom);
    await this.userRoomRepository.save(userRoom);

    return {
      success: true,
    };
  }

  async updateAsync(id: number, data: Room) {
    const room = await this.roomRepository.findOneBy({ id: id });

    if (!room)
      throw new HttpException('Room does not exits', HttpStatus.BAD_REQUEST);

    await this.roomRepository.update({ id: id }, data);

    return {
      success: true,
      room,
    };
  }

  async listUserRoomAsync(roomId: number | string) {
    const listUser = await this.roomRepository.find({
      where: { id: Number(roomId) },
      relations: ['rooms', 'rooms.userRoom'],
    });
    return listUser;
  }

  async deleteAsync(id: number) {
    const room = await this.roomRepository.findOneBy({ id: id });
    if (!room)
      throw new HttpException('Room does not exits', HttpStatus.BAD_REQUEST);

    await this.roomRepository.softDelete(id);

    return { success: true };
  }

  async joinAsync(userReq: AuthJwtPayload, data) {
    const userId = userReq.userId;

    const user = await this.userRepository.findOneBy({ id: userId });
    const room = await this.roomRepository.findOneBy({ id: data.roomId });

    if (!room)
      throw new HttpException('Room does not exits', HttpStatus.BAD_REQUEST);

    const userRoom = await this.userRoomRepository.create();
    userRoom.room = room;
    userRoom.user = user;

    await this.userRoomRepository.save(userRoom);

    return { success: true };
  }
}
