import { IsNotEmpty } from 'class-validator';
import { ChatEntity } from '../entities/chats.entity';
import { UserEntity } from '../entities/users.entity';
import { ApiProperty } from '@nestjs/swagger';

export interface Room {
  id?: number;
  roomName?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  chats?: ChatEntity[];
  users?: UserEntity[];
  user?: UserEntity;
}

export class RoomDTO implements Room {
  @ApiProperty()
  @IsNotEmpty()
  roomName: string;

  userId: number;
}
