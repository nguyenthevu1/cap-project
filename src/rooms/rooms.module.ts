import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomEntity } from '../entities/rooms.entity';
import { RoomsController } from './rooms.controller';
import { RoomsService } from './rooms.service';
import { UserEntity } from '../entities/users.entity';
import { UserRoomEntity } from './../entities/userRoom.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RoomEntity, UserEntity, UserRoomEntity])],
  controllers: [RoomsController],
  providers: [RoomsService],
  exports: [RoomsService],
})
export class RoomsModule {}
