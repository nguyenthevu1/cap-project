import {
  Body,
  Controller,
  Delete,
  Param,
  Post,
  Put,
  UseGuards,
  UsePipes,
  Req,
  Get,
} from '@nestjs/common';
import { RoomsService } from './rooms.service';
import { RoomDTO } from './room.dto';
import { ValidationPipe } from '../helpers/validation.pipe';
// import { AuthGuard } from '../helpers/auth.guard';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';

@ApiTags('Rooms')
@ApiSecurity('JWT-auth')
// @UseGuards(new AuthGuard())
@Controller('api/rooms')
export class RoomsController {
  constructor(private roomService: RoomsService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  createRoom(@Body() data: RoomDTO, @Req() req) {
    return this.roomService.createAsync(req.user, data);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  updateRoom(@Param('id') id, @Body() data: RoomDTO) {
    return this.roomService.updateAsync(id, data);
  }

  @Delete(':id')
  deleteRoom(@Param('id') id: number) {
    return this.roomService.deleteAsync(id);
  }

  @Get(':id')
  listUserOfRoom(@Param('id') id: number) {
    return this.roomService.listUserRoomAsync(id);
  }

  @Post('join')
  joinRoom(@Body() data, @Req() req) {
    return this.roomService.joinAsync(req.user, data);
  }
}
