/* eslint-disable prettier/prettier */
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
export const config: TypeOrmModuleOptions = {
  type: 'postgres',
  port: 5432,
  host: 'localhost',
  username: 'postgres',
  password: '2602',
  database: 'cap',
  entities: ['./src/**/*.entity.ts'],
  synchronize: true,
  logging: true,
};
